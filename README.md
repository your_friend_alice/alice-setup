# alice-setup

Idempotently configure a system the way I like it.

## Rrequirements

- Ubuntu 18.04+
- git
- Account with sudo access

## Usage

```
./run.sh
```

If you run into github API rate limits while installing packages from Github Releases, set `$GITHUB_AUTH` to `<your_github_username>:<your_github_password>`. If you're using 2 factor authentication, you'll need to set an app password.

## Notes

This creates a systemd service called `cdlock.service` which disables the eject button on /dev/sr0. run `sudo systemctl enable --now cdlock.service` to make your laptop less annoying.
