#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
set -ex
URL="$1"
shift
FILE="$1"
shift
HASH="$1"
shift

function checksum() {
    [[ -f "$FILE" ]] && [[ "$(sha256sum -b "$1" | cut -d ' ' -f 1)" == "$2" ]]
}

if ! checksum "$FILE" "$HASH"
then
    curl -L "$URL" > "$FILE"
fi
checksum "$FILE" "$HASH"
