#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
SRC="$REPO/.src/fuzzel"
set -ex

export DEBIAN_FRONTEND=noninteractive
apt update
apt-get install -y meson ninja-build wayland-protocols scdoc libcairo-5c0 libcairo-5c-dev libtllist-dev libpixman-1-0 libpixman-1-dev libwayland-client0 libwayland-cursor0 libwayland-dev libxkbcommon0 libxkbcommon-dev libpng-dev libpng16-16 librsvg2-2 librsvg2-dev libfcft4 libfcft-dev

sudo -u "$SUDO_USER" "$REPO/util/git.sh" 'https://codeberg.org/dnkl/fuzzel' 1.9.1 "$SRC"
cd "$SRC"
sudo -u "$SUDO_USER" rm -rf bld
sudo -u "$SUDO_USER" mkdir -p bld/release
cd bld/release
sudo -u "$SUDO_USER" meson --prefix=/usr --buildtype=release -Denable-cairo=enabled -Dpng-backend=libpng -Dsvg-backend=librsvg ../..
sudo -u "$SUDO_USER" ninja
ninja install
