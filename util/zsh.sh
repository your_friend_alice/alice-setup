#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
set -ex

for FILE in /bin/zsh /usr/bin/zsh; do
    if [[ -x "$FILE" ]]; then
        chsh -s "$FILE" "$SUDO_USER" && exit 0
    fi
done
echo 'no zsh!'
exit 1
