#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
EXTS_SRC="$REPO/.src/urxvt-exts"
set -ex

mkdir -p "$HOME/.urxvt/ext"

"$REPO/util/git.sh" 'https://github.com/simmel/urxvt-resize-font' master "$EXTS_SRC/resize-font"
ln -sf "$(readlink -f "$EXTS_SRC/resize-font/resize-font" )" "$HOME/.urxvt/ext"
