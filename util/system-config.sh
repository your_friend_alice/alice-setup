#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
set -ex

function copy() {
    SRC="$(readlink -f "$1")"
    DST="/$1"
    mkdir -p "$(dirname "$DST")"
    cp "$SRC" "$DST"
}

cd "$REPO/system"
find -type f -printf '%P\n' | while read -r FILE; do
    copy "$FILE"
done
systemctl daemon-reload
