#!/bin/bash
set -ex

[[ -x /usr/local/bin/firefox ]] && exit 0

rm -rf /opt/firefox
mkdir -p /opt/firefox
curl -L 'https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US' | tar -xjv --strip-components=1 -C /opt/firefox
chmod +x /opt/firefox/firefox
ln -s /opt/firefox/firefox /usr/local/bin/firefox
cat <<EOF > /usr/share/applications/firefox.desktop
[Desktop Entry]
Name=Firefox Stable
Comment=Web Browser
Exec=/opt/firefox/firefox %u
Terminal=false
Type=Application
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
Actions=Private;

[Desktop Action Private]
Exec=/opt/firefox/firefox --private-window %u
Name=Open in private mode
EOF
update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/local/bin/firefox 200
update-alternatives --set x-www-browser /usr/local/bin/firefox
