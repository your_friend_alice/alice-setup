#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
SRC="$REPO/.src/desktop-generator"
set -ex

export DEBIAN_FRONTEND=noninteractive
apt update && apt install python3-cairo-dev

sudo -u "$SUDO_USER" "$REPO/util/git.sh" 'https://gitlab.com/your_friend_alice/desktop-generator.git' master "$SRC"
cd "$SRC"
sudo -u "$SUDO_USER" pip3 install --user -r requirements.txt
sudo -u "$SUDO_USER" mkdir -p "/home/$SUDO_USER/.bin"
sudo -u "$SUDO_USER" ln -sf "$(readlink -f desktop-generator.py)" "/home/$SUDO_USER/.bin"
