#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
SRC="$REPO/.src/fantasque-nerd-font"
set -ex

mkdir -p "$SRC"
cd "$SRC"
"$REPO/util/remote-file.sh" \
    'https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/FantasqueSansMono.zip' \
    "$SRC/src.zip" \
    '7274bae9949a6f5100f6c5248c9b9649bd64ba3f9f1e9b9dd53a71f8fdd6d9c0'
unzip -o "$SRC/src.zip"
ls
mkdir -p "$HOME/.fonts"
for FILE in *.ttf; do
    ln -sf "$(readlink -f "$FILE")" "$HOME/.fonts/"
done
fc-cache -f -v
