#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
SRC="$REPO/.src/polybar"
set -ex

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y cmake python3-xcbgen xcb xcb-proto sphinx-common libasound2 libasound2-dev libcairo-5c0 libcairo-5c-dev libnl-genl-3-200 libnl-genl-3-dev libpulse0 libpulse-dev libxcb-composite0 libxcb-composite0-dev libxcb-image0 libxcb-image0-dev libxcb-randr0 libxcb-randr0-dev libuv1 libuv1-dev

sudo -u "$SUDO_USER" "$REPO/util/git.sh" 'https://github.com/jaagr/polybar' 3.5.4 "$SRC"
cd "$SRC"
sudo -u "$SUDO_USER" ./build.sh -a -p -n -A -j 8
cd build
make install
