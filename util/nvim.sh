#!/bin/bash
set -ex

yes '' | update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
yes '' | update-alternatives --config vi
yes '' | update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
yes '' | update-alternatives --config vim
yes '' | update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
yes '' | update-alternatives --config editor

sudo -u "$SUDO_USER" curl \
    -fLo \
    "/home/$SUDO_USER/.local/share/nvim/site/autoload/plug.vim" \
    --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

sudo -u "$USER" nvim +silent +PlugInstall +qall
