#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
set -ex

declare -a TOUCH_FILES=( "$HOME/.local.Xresources" "$HOME/.local.zshrc" )

function localVars() {
    for FILE in "$REPO/local-vars.sh" "$REPO/local-vars.example.sh"; do
        if [[ -f "$FILE" ]]; then
            printf "%s" "$FILE"
            exit 0
        fi
    done
    echo "No local vars file found"
    exit 1
}

function link() {
    SRC="$(readlink -f "$1")"
    DST="$HOME/$1"
    if ! [[ "$1" == ".gitignore" ]]; then
        mkdir -p "$(dirname "$DST")"
        ln -sf "$SRC" "$DST"
    fi
}

cd "$REPO/dotfiles"
find . -type f -printf '%P\n' | while read -r FILE; do
    link "$FILE"
done

for FILE in "${TOUCH_FILES[@]}"; do
    touch "$FILE"
done
