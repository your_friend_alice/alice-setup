#!/bin/bash
set -ex
"/home/$SUDO_USER/.bin/desktop-generator.py" -f "/home/$SUDO_USER/.Xdefaults" -s 1920x1080 --pitch 120 --scronch 0.1 /usr/share/pixmaps/pretty.png
