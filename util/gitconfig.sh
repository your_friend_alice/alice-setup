#!/bin/bash
TEMPLATE_DIR="$HOME/.config/git/template"
mkdir -p "$TEMPLATE_DIR"
echo 'ref: refs/heads/main' > "$TEMPLATE_DIR/HEAD"
git config --global init.templateDir "$TEMPLATE_DIR"
git config --global alias.cleanup "!git fetch --prune && git for-each-ref --format '%(upstream:track) %(refname:short)' refs/heads | awk '\$1 == \"[gone]\" {print \$2}' | xargs git branch -d"
git config --global alias.pr "!\"$HOME/.bin/mkpr\""
git config --global --add --bool push.autoSetupRemote true
git config --global --add --bool rerere.enabled true
git config --global column.ui auto
git config --global branch.sort -committerdate
git config --global alias.staash 'stash --all'
git config --global alias.ls "!\"$HOME/.bin/gitls\""
