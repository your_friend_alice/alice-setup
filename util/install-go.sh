#!/bin/bash
set -ex
GO_VERSION=1.23.2
YAEGI_VERSION=0.16.1
/usr/local/go/bin/go version | grep -F "go version go$GO_VERSION linux/amd64" || (rm -rf /usr/local/go && curl -s -L "https://golang.org/dl/go$GO_VERSION.linux-amd64.tar.gz" | tar -C /usr/local -zxv)
(yaegi version | grep -F "$YAEGI_VERSION") || (curl -s -L "https://github.com/traefik/yaegi/releases/download/v${YAEGI_VERSION}/yaegi_v${YAEGI_VERSION}_linux_amd64.tar.gz" | tar -C /usr/local/bin -zxv yaegi)
