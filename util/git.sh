#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
set -ex
URL="$1"
shift
BRANCH="$1"
shift
DIR="$1"
TMP="$DIR.tmp"
shift
if [[ -d "$DIR" ]]; then
    cd "$DIR"
    git fetch
    [[ -n "$BRANCH" ]] && git checkout --force "$BRANCH"
else
    rm -rf "$TMP"
    mkdir -p "$TMP"
    git clone "$URL" "$TMP"
    mv "$TMP" "$DIR"
fi
