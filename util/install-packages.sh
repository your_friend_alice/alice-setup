#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
set -ex
export DEBIAN_FRONTEND=noninteractive

apt update && apt install -y curl jq

GRP_DEB="$REPO/.deb/get-release-package.deb"
if ! [[ -f "$GRP_DEB" ]]; then
    mkdir -p "$REPO/.deb"
    curl 'https://gitlab.com/api/v4/projects/25458906/packages/generic/get-release-pkg/0.1.5/get-release-pkg.0.1.5.linux.amd64.deb' > "$GRP_DEB.tmp"
    mv "$GRP_DEB.tmp" "$GRP_DEB"
fi
apt install -y "$GRP_DEB"

cd "$REPO/.deb"
apt update && apt install -y \
    "./$(get-release-pkg --format deb --url https://github.com/sharkdp/fd)" \
    acpi \
    alsa-utils \
    blueman \
    bluez \
    bspwm \
    build-essential \
    cmake \
    compton \
    dmenu \
    docker.io \
    docker-compose \
    eog \
    evince \
    feh \
    fonts-font-awesome \
    fzf \
    gdu \
    git \
    htop \
    lf \
    libpam-gnome-keyring \
    light-locker \
    lxpolkit \
    mpv \
    neovim \
    paprefs \
    pass \
    pinentry-tty \
    python3-neovim \
    python3-pip \
    python3-virtualenv \
    qalc \
    ripgrep \
    rhythmbox \
    rlwrap \
    rustc \
    rxvt-unicode-256color \
    scrot \
    shellcheck \
    silversearcher-ag \
    software-properties-common \
    suckless-tools \
    sway \
    tmux \
    unzip \
    wl-clipboard \
    xautolock \
    xfce4-session \
    xinit \
    zsh

apt purge -y \
    thunderbird

apt -y autoremove

get-release-pkg --format tar.gz --url https://github.com/bootandy/dust | xargs tar -C /usr/local/bin --strip-components 1 --wildcards '*/dust' -xvf
