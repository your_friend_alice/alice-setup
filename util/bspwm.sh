#!/bin/bash
REPO="$(dirname "$(dirname "$(readlink -f "$0")")")"
SXHKD_SRC="$REPO/.src/sxhkd"
set -ex

sudo -u "$SUDO_USER" "$REPO/util/git.sh" 'https://github.com/baskerville/sxhkd' master "$SXHKD_SRC"

cd "$SXHKD_SRC"
sudo -u "$SUDO_USER" make
make install
