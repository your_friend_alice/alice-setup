#!/bin/bash
set -ex

# bootstrap this script to root
if [[ "$USER" != root ]]; then
    sudo "$0" "$@"
    systemctl --user daemon-reload
    exit $?
fi

REPO="$(dirname "$(readlink -f "$0")")"
cd "$REPO"
                util/install-packages.sh
                util/install-firefox.sh
                util/install-go.sh
                util/zsh.sh
                util/polybar.sh
                util/fuzzel.sh
sudo -u "$SUDO_USER" util/urxvt-exts.sh
sudo -u "$SUDO_USER" util/symlink.sh
sudo -u "$SUDO_USER" util/fonts.sh
                util/desktop-generator.sh
                util/lightdm.sh
                util/bspwm.sh
                util/system-config.sh
                util/nvim.sh
sudo -u "$SUDO_USER" util/gitconfig.sh
