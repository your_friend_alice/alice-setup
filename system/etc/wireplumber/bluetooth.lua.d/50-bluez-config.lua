bluez_monitor.enabled = true

bluez_monitor.properties = {
  ["bluez5.enable-hw-volume"] = false,
  ["bluez5.headset-roles"] = "[]",
  ["bluez5.hfphsp-backend"] = "none",
  ["with-logind"] = false,
}

bluez_monitor.rules = {
  {
    matches = {
      {
        -- This matches all cards.
        { "device.name", "matches", "bluez_card.*" },
      },
    },
    apply_properties = {
      ["bluez5.auto-connect"]  = "[ a2dp_sink ]",
      ["device.profile"] = "a2dp-sink",
    },
  },
}
