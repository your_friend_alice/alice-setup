local source_cmd = "source " .. string.format("%s/legacy.vim", vim.fn.stdpath("config"))
vim.cmd(source_cmd)
vim.fn.stdpath("config")
require('gitsigns').setup()
