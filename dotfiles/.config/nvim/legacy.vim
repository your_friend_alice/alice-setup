"
"Plugins
call plug#begin()
Plug 'https://github.com/iCyMind/NeoSolarized'
Plug 'https://github.com/sirtaj/vim-openscad.git'
Plug 'https://github.com/tpope/vim-surround'
Plug 'https://github.com/vim-airline/vim-airline'
Plug 'https://github.com/vim-airline/vim-airline-themes'
Plug 'https://github.com/jiangmiao/auto-pairs'
Plug 'https://github.com/hashivim/vim-terraform.git'
Plug 'https://github.com/euclio/vim-markdown-composer', { 'do': 'cargo build --release -j8' }
Plug 'https://github.com/elzr/vim-json'
Plug 'https://github.com/rhysd/conflict-marker.vim'
Plug 'https://github.com/mustache/vim-mustache-handlebars'
Plug 'https://github.com/reedes/vim-pencil'
Plug 'https://github.com/tikhomirov/vim-glsl.git'
Plug 'https://github.com/fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'https://github.com/rust-lang/rust.vim'
Plug 'https://github.com/vim-syntastic/syntastic'
Plug 'https://github.com/stephpy/vim-yaml'
Plug 'https://github.com/towolf/vim-helm'
Plug 'https://github.com/lewis6991/gitsigns.nvim', { 'tag': 'v0.8.0' }
Plug 'https://github.com/towolf/vim-helm'
call plug#end()

let g:pencil#wrapModeDefault = 'soft'
let g:pencil#cursorwrap = 0
augroup pencil
  autocmd!
  autocmd FileType markdown,mkd call pencil#init()
  autocmd FileType text         call pencil#init()
augroup END

let g:rustfmt_autosave = 1

"easy window navigation
:tnoremap <A-h> <C-\><C-N><C-w>h
:tnoremap <A-j> <C-\><C-N><C-w>j
:tnoremap <A-k> <C-\><C-N><C-w>k
:tnoremap <A-l> <C-\><C-N><C-w>l
:inoremap <A-h> <C-\><C-N><C-w>h
:inoremap <A-j> <C-\><C-N><C-w>j
:inoremap <A-k> <C-\><C-N><C-w>k
:inoremap <A-l> <C-\><C-N><C-w>l
:nnoremap <A-h> <C-w>h
:nnoremap <A-j> <C-w>j
:nnoremap <A-k> <C-w>k
:nnoremap <A-l> <C-w>l
:tnoremap <Esc> <C-\><C-n>

"looks
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set background=dark
silent! colorscheme NeoSolarized
set et
set shiftwidth=4
set tabstop=4
let g:airline_powerline_fonts=1
set ruler
set number

"retab command
:command! -range=% -nargs=0 S execute '<line1>,<line2>s#^\t\+#\=repeat(" ", len(submatch(0))*' . &ts . ')'
:command! -range=% -nargs=0 T execute '<line1>,<line2>s#^\( \{'.&ts.'\}\)\+#\=repeat("\t", len(submatch(0))/' . &ts . ')'

"tweaks
set confirm "before quitting unsaved
set pastetoggle=<F2>

"completion?
set wildmenu

"search stuff
set hlsearch
set ignorecase
set smartcase

"persistent clipboard
set clipboard+=unnamedplus
set viminfo+=n/tmp/$USER.nviminfo

"syntax
autocmd BufRead,BufNewFile *.md	       set filetype=markdown
autocmd BufRead,BufNewFile *.scad      set filetype=openscad
autocmd BufRead,BufNewFile Jenkinsfile* set filetype=groovy
autocmd BufRead,BufNewFile Fastfile    set filetype=ruby

"indentation
filetype plugin indent on

"folding
set foldmethod=indent
set foldlevel=99

"markdown
cabbrev md ComposerOpen

"FE syntax config
autocmd FileType ruby     setlocal ts=2 sw=2 expandtab
autocmd FileType yaml     setlocal ts=2 sw=2 expandtab
autocmd FileType helm     setlocal ts=2 sw=2 expandtab
autocmd FileType eruby    setlocal ts=2 sw=2 expandtab
autocmd FileType groovy   setlocal ts=2 sw=2 expandtab
autocmd FileType markdown setlocal ts=4 sw=4 expandtab
autocmd FileType json     setlocal ts=2 sw=2 expandtab
autocmd FileType tf       setlocal ts=2 sw=2 expandtab
autocmd FileType go       setlocal ts=2 sw=2 noexpandtab
autocmd FileType go       map <C-c> :w<CR>:GoBuild<CR>

set cursorline
hi Normal guibg=NONE ctermbg=NONE
unmap Y

set mouse=
" Disable mouse
