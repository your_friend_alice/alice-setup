#!/usr/bin/env bash
set -eou pipefail
if [[ "$#" != 2 ]]; then
    echo "Usage: $0 <kicad|openscad|empty> <NAME>"
    exit 1
fi
TYPE="$1"
NAME="$2"
DIR="$HOME/src/gitlab.com/your_friend_alice/$NAME"

mkdir -p "$DIR"
cd "$DIR"
git init
if ! glab repo view "$NAME"; then
    glab repo create --name "$NAME" --private --defaultBranch main
fi
case "$TYPE" in
    kicad)
        cat > "$DIR/.gitignore" <<EOF
/*-backups
/gerber/*
*.zip
/fp-info-cache
*-bak
*.kicad_prl
*.net
*.stl
~*.lck
EOF
    ;;
    openscad)
        cat > "$DIR/.gitignore" <<EOF
/dependencies/
/out/
EOF
        cat > "$DIR/project.yaml" <<EOF
version: 1
openscad:
  version: ">= 2023.12.09"
  features:
    - manifold
    - roof
default:
  file: model.scad
  params:
    "\$fn": 90
render:
  main: {params: {render_main: true}}
dependencies: {}
#   lib: "git+https://gitlab.com/your_friend_alice/openscad-lib.git#main"
EOF
        cat > "$DIR/model.scad" <<EOF
cube(10);
EOF
    ;;
    *)
        touch "$DIR/.gitignore"
    ;;
esac
git add "$DIR/.gitignore" "$DIR/"
git commit -m "initialize defaults for $TYPE project"
git push
glab repo view --web
