#source GRML file
[[ -f ~/.grml.zshrc ]] && source ~/.grml.zshrc

export EDITOR=nvim
#enable vim keybindings
bindkey -v

#set paths
PATH="$HOME/.local/bin:$HOME/.bin:$HOME/.go/bin:$HOME/.cabal/bin:/usr/local/go/bin:$HOME/.gem/bin:$PATH"
export GOPATH="$HOME/.go"
export GEM_HOME="$HOME/.gem"

#set terminal
test $COLORTERM = "yes" && test $TERM = "xterm" && TERM=xterm-256color

sshff() {
    ssh "$1" 'DISPLAY=:0 xargs firefox'
}
function cal {
    if [ -t 1 ] ; then ncal -b "$@"; else /usr/bin/cal "$@"; fi
}
alias open="xdg-open"
alias xclip="xclip -selection clipboard"
alias gg='git log --oneline --abbrev-commit --all --graph --decorate --color'
alias vim=nvim
alias gocli='rlwrap yaegi'
export GPG_TTY=$(tty)
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down
function vimtmp {
    tmpfile="$(mktemp --suffix ".$1")"
    cat > "$tmpfile"
    shift
    "$EDITOR" "$tmpfile" $@
    cat "$tmpfile"
    rm "$tmpfile"
}
function hr {
    for i in {1..$COLUMNS}; do
        printf "\\033[0;45m "
    done
    tput sgr0
}
function calc {
    LINE=""
    if [[ $# -gt 0 ]]; then
        LINE="$(echo "$@")"
    else
        read -r LINE
    fi
    qalc --color "$LINE" >&2
    qalc -t "$LINE" | cut -d ' ' -f 1
}
alias d8='date --rfc-3339 s'
alias kcc='kubectl config use-context'

autoload -U colors && colors

function kube_context_prompt () {
    local data="$(grep -Po '(?<=current-context: ).+' < ~/.kube/config)"
    REPLY=""
    [[ -n "$data" ]] && REPLY="%F{magenta}(%fkube%F{magenta})%F{yellow}-%F{magenta}[%F{cyan}$data%F{magenta}]%F{reset_color%} "
}
if which kubectl >/dev/null; then
    grml_theme_add_token kube-context -f kube_context_prompt '' ''
fi

#zstyle ':vcs_info:git*' formats "%{${fg[magenta]}%}[%{${fg[green]}%}git%{${fg[magenta]}%}:%{${fg[green]}%}%b%{${fg[magenta]}%}]%{$reset_color%} "
zstyle ':prompt:grml:left:setup' items user at host path vcs kube-context percent

which kubectl >/dev/null && source <(kubectl completion zsh)
which stern >/dev/null && source <(stern --completion=zsh)
which arduino-cli >/dev/null && source <(arduino-cli completion zsh)
which oscadpkg >/dev/null && source <(oscadpkg completion zsh)
# there's a synthesizer named "helm" we wanna avoid running
[[ "$(which helm)" != /usr/bin/helm ]] >/dev/null && source <(helm completion zsh 2>/dev/null)

#source local zshrc
[[ -f ~/.local.zshrc ]] && source ~/.local.zshrc

compdef _kubectl kc=kubectl

if [[ -f "/usr/lib/google-cloud-sdk/completion.bash.inc" ]]; then
    autoload bashcompinit
    bashcompinit
    source /usr/lib/google-cloud-sdk/completion.bash.inc
fi

alias t="mkcd $HOME/tmp"
if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
    export MOZ_ENABLE_WAYLAND=1
    export QT_QPA_PLATFORM=wayland
else
    MOZ_X11_EGL=1
fi
export HISTSIZE=9999999
setopt hist_ignore_all_dups

function mkrepo {
    "$HOME/.bin/mkrepo.sh" "$@" && cd "$HOME/src/gitlab.com/your_friend_alice/$2"
}

function lim {
    sed "s/\(.\{${1:-500}\}\).*/\1/"
}
